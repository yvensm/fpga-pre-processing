library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
library std;
use std.textio.all;

entity serializer_tb is
end serializer_tb;


architecture tb of serializer_tb is

component serializer is
	port(
		clk: in std_logic;
		reset: in std_logic;
		data_in: in std_logic_vector(31 downto 0);
		load: in std_logic;
		enable: in std_logic;
		
		
		data_out: out std_logic_vector(7 downto 0);
		ctrl: out std_logic;
		valid: out std_logic;
		finish: out std_logic
	);
end component;


signal		clk: std_logic:='0';
signal		reset: std_logic:='1';
signal		data_in: std_logic_vector(31 downto 0);
signal		load: std_logic:='0';
signal		enable: std_logic:='1';
		
		
signal		data_out: std_logic_vector(7 downto 0);
signal		ctrl: std_logic;
signal		valid: std_logic;
signal		finish: std_logic;

file	dados	: text;

begin

serializer_map: serializer port map(clk,reset,data_in,load,enable,data_out,ctrl,valid,finish);

clk<= not clk after 20ps;
process
	variable status: file_open_status;
	variable l: line;
	variable linedata: BIT_VECTOR(31 DOWNTO 0);
begin
	file_open(status,dados,"dados.txt",read_mode);
	assert status = open_ok
		report "Nao foi possivel abrir"
		severity failure;
	while not(endfile(dados)) loop
		readline(dados, l);
		read(l,linedata);
		data_in<= to_stdlogicvector(linedata);
		load<='1';
		wait for 100ps;
	end loop;
	wait;	
end process;
end tb;