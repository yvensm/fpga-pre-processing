library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity serializer is
	port(
		clk: in std_logic;
		reset: in std_logic;
		data_in: in std_logic_vector(31 downto 0);
		load: in std_logic;
		enable: in std_logic;
		
		
		data_out: out std_logic_vector(7 downto 0);
		ctrl: out std_logic;
		valid: out std_logic;
		finish: out std_logic
	);
end serializer;

architecture behavioral of serializer is 

signal counter_cycle : std_logic_vector(2 downto 0):= (others => '0');
signal dataInput: std_logic_vector(31 downto 0):= (others => '0');
signal dataOutput: std_logic_vector(7 downto 0):= (others => '0');
signal valid_sig: std_logic:='0';
signal finish_sig: std_logic:= '0';


begin

counter: process(clk, reset,enable)
			begin
				if (reset = '0')then
					counter_cycle<= (others => '0');
				elsif (enable = '1') then
					if (clk'event and clk='1') then
						if(counter_cycle = 5) then
							counter_cycle<= (others => '0');
						else
							counter_cycle<= counter_cycle + 1;
						end if;
					end if;
				end if;
			end process;
			
			
dataIn: 	process(clk,reset,enable,counter_cycle) 
			begin
				if(reset = '0') then
					dataInput<= (others => '0');
					--counter_cycle<= "000";
					--valid_sig <= '0';
				
				elsif (enable = '1') then
					if (clk'event and clk='1') then
						if(load = '1' and counter_cycle = 0) then
							dataInput<= data_in;
							--counter_cycle<= counter_cycle + 1;
						end if;
					end if;
				end if;
			end process;
			
dataOut: process(clk,reset,enable,counter_cycle)
			begin
				if (reset = '0') then
					--dataOutput<= (others => '0');
					valid_sig<='0';
					finish_sig<='0';
				elsif (enable= '1') then
					if(clk'event and clk ='1')then
						if(counter_cycle = 1) then
							valid_sig<='1';
							finish_sig<='0';
						elsif(counter_cycle = 5) then
							finish_sig<='1';
							valid_sig<='1';
						end if;
					end if;
				end if;
			end process;
			
with counter_cycle select
	dataOutput <=	dataInput(31 downto 24) when "010", 
					dataInput(23 downto 16) when "011",
					dataInput(15 downto 8) when "100",
					dataInput(7 downto 0) when "101",
					(others => '0') when others;
			
valid<=valid_sig;
finish<=finish_sig;
data_out<=dataOutput;

end behavioral;